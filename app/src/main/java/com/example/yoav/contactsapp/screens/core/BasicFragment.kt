package com.example.yoav.contactsapp.screens.core

import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by yoav on 02/02/2018
 */

abstract class BasicFragment : DaggerFragment() {
    private var compositeDisposable = CompositeDisposable()

    open fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
        compositeDisposable.dispose()
    }

    override fun onStart() {
        super.onStart()
        if (compositeDisposable.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }

    }

    open fun onBackPressed(): Boolean {
        return false
    }
}