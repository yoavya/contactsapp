package com.example.yoav.contactsapp.screens

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.yoav.contactsapp.R
import com.example.yoav.contactsapp.screens.contacts.ContactsContainerFragment
import com.example.yoav.contactsapp.screens.core.BasicFragment
import com.example.yoav.contactsapp.services.MessagingService
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity_layout)
        startMessagingService()
        presenter.init()
    }

    private fun startMessagingService() {
        val messagingService = MessagingService()
        val serviceIntent = Intent(applicationContext, messagingService::class.java)
        if (!isMyServiceRunning(messagingService::class.java)) {
            startService(serviceIntent)
        }
    }

    @Suppress("DEPRECATION")
    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        return manager.getRunningServices(Integer.MAX_VALUE).any { serviceClass.name == it.service.className }
    }

    override fun swapToContractsFragment() {
        swapFragment(ContactsContainerFragment.newInstance())
    }

    override fun swapFragment(fragment: BasicFragment, pushToBackStack: Boolean, add: Boolean) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (add) {
            fragmentTransaction.add(R.id.fragment_container, fragment)
        } else {
            fragmentTransaction.replace(R.id.fragment_container, fragment)
        }
        if (pushToBackStack) {
            fragmentTransaction.addToBackStack(fragment.tag)
        }
        fragmentTransaction.commit()

    }

    override fun onBackPressed() {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as BasicFragment
        if (currentFragment.onBackPressed()) {
            return
        }
        super.onBackPressed()
    }
}