package com.example.yoav.contactsapp.di

import com.example.yoav.contactsapp.application.ContactsApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Singleton

/**
 * Created by yoav on 02/02/2018
 */

@Singleton
@Component(modules = arrayOf(AppModule::class, AndroidInjectionModule::class, ActivityBuilder::class))
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: ContactsApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: ContactsApplication)
}