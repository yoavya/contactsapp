package com.example.yoav.contactsapp.screens.contacts.di

import com.example.yoav.contactsapp.screens.contacts.ContactsContract
import com.example.yoav.contactsapp.screens.contacts.ContactsFragment
import com.example.yoav.contactsapp.screens.contacts.ContactsPresenter
import dagger.Binds
import dagger.Module

/**
 * Created by yoav on 02/02/2018
 */

@Module
abstract class ContactsFragmentModule {
    @Binds
    abstract fun providesContactsFragment(fragment: ContactsFragment): ContactsContract.View


    @Binds
    abstract fun providesContactsPresenter(presenter: ContactsPresenter): ContactsContract.Presenter
}

