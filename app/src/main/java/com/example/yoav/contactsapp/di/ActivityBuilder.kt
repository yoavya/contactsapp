package com.example.yoav.contactsapp.di

import com.example.yoav.contactsapp.screens.MainActivity
import com.example.yoav.contactsapp.screens.di.MainActivityFragmentProvider
import com.example.yoav.contactsapp.screens.di.MainActivityModule
import com.example.yoav.contactsapp.services.MessagingService
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by yoav on 02/02/2018
 */
@Module
abstract class ActivityBuilder {
    @PerActivity
    @ContributesAndroidInjector(modules = [(MainActivityModule::class), (MainActivityFragmentProvider::class)])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindMessagingService(): MessagingService
}

