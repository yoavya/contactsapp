package com.example.yoav.contactsapp.screens

import com.example.yoav.contactsapp.di.PerActivity
import com.example.yoav.contactsapp.screens.core.BasicFragment
import javax.inject.Inject

/**
 * Created by yoav on 02/02/2018
 */
@PerActivity
class MainPresenter @Inject constructor(val view: MainContract.View) : MainContract.Presenter {
    override fun init() {
        view.swapToContractsFragment()
    }

    override fun swapFragment(fragment: BasicFragment, pushToBackStack: Boolean, add: Boolean) {
        view.swapFragment(fragment, pushToBackStack, add)
    }

}