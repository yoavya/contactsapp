package com.example.yoav.contactsapp.data.local.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class LocalDataContact : RealmObject() {

    @PrimaryKey
    var id: Int? = null

    var firstName: String? = null

    var lastName: String? = null

    var email: String? = null

    var gender: String? = null

    var phone: String? = null

    var address: String? = null

    var job: String? = null

    var avatar: String? = null

    var date: Long? = null
}