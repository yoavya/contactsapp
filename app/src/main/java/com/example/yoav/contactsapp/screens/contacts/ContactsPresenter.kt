package com.example.yoav.contactsapp.screens.contacts

import com.example.yoav.contactsapp.data.Repository
import com.example.yoav.contactsapp.screens.MainContract
import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import com.example.yoav.contactsapp.screens.info.ContactInfoFragment
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ContactsPresenter
@Inject
constructor(val view: ContactsContract.View, private val repo: Repository, private val mainPresenter: MainContract.Presenter) :
        ContactsContract.Presenter {


    override fun initContacts() {
        view.displayLoadingIndicator()
        updateContactsList()
        val contactClickListener = PublishSubject.create<Int>()
        view.setContactClickListener(contactClickListener)

        contactClickListener
                .doOnSubscribe{ view.addDisposable(it)}
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext{mainPresenter.swapFragment(ContactInfoFragment.newInstance(it), true, true)}
                .subscribe()

    }

    private fun updateContactsList(forceRefresh: Boolean = false) {
        when (view.getTabType()) {
            ContactsFragment.RECENT_TAB_TYPE ->
                repo.getRecentContactsAsync()
                        .compose({ toViewSequence(it) })
                        .subscribe()

            ContactsFragment.ALL_CONTACTS_TAB_TYPE ->
                repo.getContactsAsync(forceRefresh)
                        .compose({ toViewSequence(it) })
                        .subscribe()

        }
    }

    override fun refreshItems() {
        updateContactsList(true)
        view.stopRefresh()
    }

    private fun toViewSequence(it: Single<List<ViewContact>>): Single<List<ViewContact>> {
        return it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { view.updateContacts(it) }
                .doOnSuccess { view.hideLoadingIndicator()}
                .doOnSubscribe({ view.addDisposable(it) })
    }

}

