package com.example.yoav.contactsapp.screens.contacts.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.yoav.contactsapp.R
import de.hdodenhof.circleimageview.CircleImageView
import kotterknife.bindView

class ContactViewHolder(viewHolder: View) : RecyclerView.ViewHolder(viewHolder) {

    val avatar: CircleImageView by bindView(R.id.contact_image)
    val name: TextView by bindView(R.id.contact_name)
    val lastContacted: TextView by bindView(R.id.last_contacted)
}