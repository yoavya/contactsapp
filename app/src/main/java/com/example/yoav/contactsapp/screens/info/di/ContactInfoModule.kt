package com.example.yoav.contactsapp.screens.info.di

import com.example.yoav.contactsapp.screens.info.ContactInfoContract
import com.example.yoav.contactsapp.screens.info.ContactInfoFragment
import com.example.yoav.contactsapp.screens.info.ContactInfoPresenter
import dagger.Binds
import dagger.Module

/**
 * Created by yoav_ on 03/02/2018
 */
@Module
abstract class ContactInfoModule {

    @Binds abstract fun providesContactInfoFragment(view: ContactInfoFragment) : ContactInfoContract.View

    @Binds abstract fun providesContactInfoPresenter(presenter: ContactInfoPresenter) : ContactInfoContract.Presenter
}

