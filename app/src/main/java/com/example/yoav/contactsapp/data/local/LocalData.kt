package com.example.yoav.contactsapp.data.local

import com.example.yoav.contactsapp.data.local.model.LocalDataContact
import com.example.yoav.contactsapp.data.remote.model.RemoteDataContact
import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import io.reactivex.Single

/**
 * Created by yoav on 03/02/2018
 */
interface LocalData {
    fun saveContactSync(contact: RemoteDataContact) : LocalDataContact
    fun getContactsAsync() : Single<List<LocalDataContact>>
    fun getContactInfoAsync(contactId: Int): Single<LocalDataContact>
    fun getContactsSortedByDateAsync(): Single<List<LocalDataContact>>
    fun updateContactDateAsync(id: Int?, date: Long)
}

