package com.example.yoav.contactsapp.data.remote

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by yoav on 03/02/2018
 */

@Singleton
class ApiFactory @Inject constructor() : RemoteData {
    private val retrofit by lazy {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
        Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://s3.eu-central-1.amazonaws.com/roboteam/")
                .client(httpClient.build())
                .build()
    }

    val api by lazy {
        retrofit.create(ContactsApi::class.java)
    }

    override fun getRemoteApi(): ContactsApi = api
}
