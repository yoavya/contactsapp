package com.example.yoav.contactsapp.data

import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import com.example.yoav.contactsapp.screens.info.model.ViewContactInfo
import io.reactivex.Single

interface Repository {
    fun getContactsAsync(forceRefresh: Boolean = false): Single<List<ViewContact>>
    fun getRecentContactsAsync(): Single<List<ViewContact>>
    fun getContactInfo(contactId: Int): Single<ViewContactInfo>
    fun updateContactDate(id: Int?)

}