package com.example.yoav.contactsapp.screens.info

import com.example.yoav.contactsapp.communication.MessagingManager
import com.example.yoav.contactsapp.data.Repository
import com.example.yoav.contactsapp.screens.info.model.ViewContactInfo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class ContactInfoPresenter @Inject constructor(val view: ContactInfoContract.View,
                                               private val repo: Repository,
                                               private val messagingManager: MessagingManager):
        ContactInfoContract
.Presenter {


    lateinit var contact: ViewContactInfo

    override fun init(contactId: Int?) {
        view.displayLoadingIndicator()
        if (contactId != null) {
            repo.getContactInfo(contactId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe{ view.addDisposable(it)}
                    .doOnSuccess { contact = it}
                    .doOnSuccess { view.setAvatar(it.avatar)}
                    .doOnSuccess { view.setName(it.name)}
                    .doOnSuccess { view.setPhone(it.phone)}
                    .doOnSuccess { view.setGender(it.gender)}
                    .doOnSuccess { view.setEmail(it.email)}
                    .doOnSuccess { view.setAddress(it.address)}
                    .doOnSuccess { view.hideLoadingIndicator()}
                    .subscribe()
        }
    }

    override fun sendMessage() {
        messagingManager.publish(String.format(Locale.getDefault(), "Hello %s", contact.name))
        repo.updateContactDate(contact.id)
    }

    override fun backClicked() {
        view.exitFragment()
    }
}