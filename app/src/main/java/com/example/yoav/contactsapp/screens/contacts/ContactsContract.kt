package com.example.yoav.contactsapp.screens.contacts

import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

interface ContactsContract {
    interface View {
        fun getTabType(): Int
        fun updateContacts(contacts: List<ViewContact>)
        fun addDisposable(disposable: Disposable)
        fun setContactClickListener(contactClickListener: PublishSubject<Int>)
        fun hideLoadingIndicator()
        fun displayLoadingIndicator()
        fun stopRefresh()
    }

    interface Presenter {
        fun initContacts()
        fun refreshItems()

    }

}