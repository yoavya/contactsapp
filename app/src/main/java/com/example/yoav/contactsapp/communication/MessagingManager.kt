package com.example.yoav.contactsapp.communication

import com.pubnub.api.PubNub
import com.pubnub.api.callbacks.SubscribeCallback

/**
 * Created by yoav on 04/02/2018
 */
interface MessagingManager {
    fun getManager(): PubNub
    fun init()
    fun subscribe()
    fun unsubscribe()
    fun setListener(listener: SubscribeCallback)
    fun removeListener()
    fun reuseListener()
    fun publish(message: String)
}