package com.example.yoav.contactsapp.communication

import com.pubnub.api.PNConfiguration
import com.pubnub.api.PubNub
import com.pubnub.api.callbacks.SubscribeCallback
import javax.inject.Inject
import javax.inject.Singleton
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.PNPublishResult
import com.pubnub.api.callbacks.PNCallback




@Singleton
class PubNubManager @Inject constructor() : MessagingManager {

    lateinit var pubnub: PubNub

    override fun getManager() = pubnub

    override fun init() {

        val pnConfiguration = PNConfiguration()
        pnConfiguration.subscribeKey = "sub-c-b30678f8-092d-11e8-be21-ca57643e6300"
        pnConfiguration.publishKey = "pub-c-d588f639-fb65-4cab-b0e7-493a0a50bd58"

        pubnub = PubNub(pnConfiguration)
    }

    override fun subscribe() {
        pubnub.subscribe()
                .channels(listOf("my_channel")) // subscribe to channels
                .execute()
    }

    override fun unsubscribe() {
        pubnub.unsubscribe()
                .channels(listOf("my_channel"))
                .execute()
    }

    private var channelListener: SubscribeCallback? = null

    override fun setListener(listener: SubscribeCallback) {
        channelListener = listener
        pubnub.addListener(listener)
    }

    override fun removeListener() {
        if (channelListener != null) {
            pubnub.removeListener(channelListener)
        }
    }

    override fun reuseListener() {
        if (channelListener != null) {
            pubnub.addListener(channelListener)
        }
    }

    override fun publish(message :String) {
        unsubscribe()
        pubnub.publish()
                .message(message)
                .channel("my_channel")
                .async(object : PNCallback<PNPublishResult>() {
                    override fun onResponse(result: PNPublishResult, status: PNStatus) {
                        subscribe()
                        // handle publish result, status always present, result if successful
                        // status.isError to see if error happened
                    }
                })
    }

}