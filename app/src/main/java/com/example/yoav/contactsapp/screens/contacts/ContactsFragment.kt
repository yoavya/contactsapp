package com.example.yoav.contactsapp.screens.contacts

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.yoav.contactsapp.R
import com.example.yoav.contactsapp.screens.contacts.adapter.ContactsAdapter
import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import com.example.yoav.contactsapp.screens.core.BasicFragment
import io.reactivex.subjects.PublishSubject
import kotterknife.bindView
import javax.inject.Inject

/**
 * Created by yoav on 02/02/2018
 */
class ContactsFragment : BasicFragment(), ContactsContract.View {
    companion object {

        private const val TAB_TYPE_KEY = "tab type key"
        const val RECENT_TAB_TYPE = 1
        const val ALL_CONTACTS_TAB_TYPE = 2
        const val RECENT_TAB_NAME = "RECENTS"
        const val ALL_CONTACTS_TAB_NAME = "CONTACTS"
        fun newInstance(tabType: Int): ContactsFragment {
            val args = Bundle()
            args.putInt(TAB_TYPE_KEY, tabType)
            val fragment = ContactsFragment()
            fragment.arguments = args
            return fragment
        }

    }
    @Inject
    lateinit var presenter: ContactsContract.Presenter

    private val contactsRecyclerView: RecyclerView by bindView(R.id.contact_list)
    private val loadingIndicator: ProgressBar by bindView(R.id.loading_indicator)
    private val pullToRefresh: SwipeRefreshLayout by bindView(R.id.swipe_to_refresh)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.contacts_fragment_layout, container, false)
    }

    private var isInit: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initContacts()
    }

    private fun initContacts() {
        if (!isInit) {
            presenter.initContacts()
            isInit = true
        }
    }

    override fun onStart() {
        super.onStart()
        initContacts()
    }

    override fun onStop() {
        super.onStop()
        isInit = false
    }

    override fun setContactClickListener(contactClickListener: PublishSubject<Int>) {
        val contactsAdapter = contactsRecyclerView.adapter as ContactsAdapter
        contactsAdapter.setContactClickListener(contactClickListener)
        contactsRecyclerView.adapter.notifyDataSetChanged()
    }

    private fun initRecyclerView() {
        pullToRefresh.setOnRefreshListener { presenter.refreshItems() }
        contactsRecyclerView.layoutManager = LinearLayoutManager(context)
        contactsRecyclerView.adapter = ContactsAdapter(emptyList())
    }

    override fun stopRefresh() {
        pullToRefresh.isRefreshing = false
    }

    override fun hideLoadingIndicator() {
        loadingIndicator.visibility = View.GONE
    }

    override fun displayLoadingIndicator() {
        loadingIndicator.visibility = View.VISIBLE
    }

    override fun updateContacts(contacts: List<ViewContact>) {
        val contactsAdapter = contactsRecyclerView.adapter as ContactsAdapter
        contactsAdapter.contacts = contacts
        contactsAdapter.notifyDataSetChanged()
    }

    override fun getTabType() = arguments?.getInt(TAB_TYPE_KEY) ?: ALL_CONTACTS_TAB_TYPE
}
