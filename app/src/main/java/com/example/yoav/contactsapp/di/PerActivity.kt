package com.example.yoav.contactsapp.di

import java.lang.annotation.RetentionPolicy
import javax.inject.Scope

/**
 * Created by yoav on 03/02/2018
 */
@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class PerActivity