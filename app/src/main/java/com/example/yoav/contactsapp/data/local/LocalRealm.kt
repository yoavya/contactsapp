package com.example.yoav.contactsapp.data.local

import com.example.yoav.contactsapp.data.local.model.LocalDataContact
import com.example.yoav.contactsapp.data.remote.model.RemoteDataContact
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.Sort
import javax.inject.Inject

class LocalRealm @Inject constructor() : LocalData {
    override fun updateContactDateAsync(id: Int?, date: Long) {
        Single.fromCallable {
            val realm = Realm.getDefaultInstance()
            val realmItem = realm.where(LocalDataContact::class.java)
                    .equalTo("id", id)
                    .findFirst()
            val localDataContact = realm.copyFromRealm(realmItem)
            localDataContact?.date = date
            realm.executeTransaction { transaction ->
                transaction.copyToRealmOrUpdate(localDataContact)
            }
            realm.close()
        }
                .subscribeOn(Schedulers.io())
                .subscribe()
    }

    override fun getContactInfoAsync(contactId: Int): Single<LocalDataContact> {
        return Single.fromCallable {
            val realm = Realm.getDefaultInstance()
            val findFirst = realm.where(LocalDataContact::class.java)
                    .equalTo("id", contactId)
                    .findFirst()
            var localDataContact = LocalDataContact()
            if (findFirst != null) {
                localDataContact = realm.copyFromRealm(findFirst)
            }
            realm.close()
            return@fromCallable localDataContact
        }
    }

    override fun getContactsSortedByDateAsync(): Single<List<LocalDataContact>> {
        return Single.fromCallable {
            val realm = Realm.getDefaultInstance()
            val realmResults = realm.where(LocalDataContact::class.java)
                    .sort("date", Sort.DESCENDING)
                    .findAll()
            val res = realm.copyFromRealm(realmResults)
            realm.close()
            res
        }
    }

    override fun saveContactSync(contact: RemoteDataContact): LocalDataContact {
        val currentLocalContact = getContactInfoAsync(contact.id!!).blockingGet()
        val realm = Realm.getDefaultInstance()
        val localDataContact = toLocalDataContact(contact, currentLocalContact?.date)
        realm.executeTransaction { transaction ->
            transaction.copyToRealmOrUpdate(localDataContact)
        }
        realm.close()
        return localDataContact
    }

    private fun toLocalDataContact(contact: RemoteDataContact, date: Long?): LocalDataContact {
        val localDataContact = LocalDataContact()
        localDataContact.id = contact.id
        localDataContact.firstName = contact.firstName
        localDataContact.lastName = contact.lastName
        localDataContact.address = contact.address
        localDataContact.avatar = contact.avatar
        localDataContact.date = date ?: contact.date?.toLong()
        localDataContact.gender = contact.gender
        localDataContact.email = contact.email
        localDataContact.job = contact.job
        localDataContact.phone = contact.phone
        return localDataContact
    }

    override fun getContactsAsync(): Single<List<LocalDataContact>> {
        return Single.fromCallable {
            val realm = Realm.getDefaultInstance()
            val results = realm.where(LocalDataContact::class.java).findAll()
            val itemsFromRealm = realm.copyFromRealm(results)
            realm.close()
            itemsFromRealm
        }
    }
}