package com.example.yoav.contactsapp.screens.info.model

data class ViewContactInfo(val id: Int?, val avatar: String?, val name: String, val phone: String?, val gender: String?,
                           val email: String?, val address: String?)