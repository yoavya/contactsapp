package com.example.yoav.contactsapp.data.remote

/**
 * Created by yoav on 03/02/2018
 */
interface RemoteData {

    fun getRemoteApi(): ContactsApi
}