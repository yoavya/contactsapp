package com.example.yoav.contactsapp.screens

import com.example.yoav.contactsapp.screens.core.BasicFragment

/**
 * Created by yoav on 02/02/2018
 */
interface MainContract {
    interface View {
        fun swapToContractsFragment()
        fun swapFragment(fragment: BasicFragment, pushToBackStack: Boolean = false, add: Boolean = false)

    }

    interface Presenter {
        fun init()

        fun swapFragment(fragment: BasicFragment, pushToBackStack: Boolean = false, add: Boolean = false)
    }
}