package com.example.yoav.contactsapp.screens.contacts.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import com.example.yoav.contactsapp.screens.contacts.ContactsFragment

class ContactsPagerAdapter(fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager) {

    private val dataSet: List<Pair<Fragment, String>> by lazy {
        listOf(
                ContactsFragment.newInstance(ContactsFragment.RECENT_TAB_TYPE)
                        to ContactsFragment.RECENT_TAB_NAME,
                ContactsFragment.newInstance(ContactsFragment.ALL_CONTACTS_TAB_TYPE)
                        to ContactsFragment.ALL_CONTACTS_TAB_NAME
        )
    }


    private val fragments = dataSet.map { it.first }

    private val tabNames = dataSet.map { it.second }

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = fragments.size

    override fun getPageTitle(position: Int) = tabNames[position]
}