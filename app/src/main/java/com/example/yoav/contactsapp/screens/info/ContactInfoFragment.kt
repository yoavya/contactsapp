package com.example.yoav.contactsapp.screens.info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.example.yoav.contactsapp.R
import com.example.yoav.contactsapp.screens.core.BasicFragment
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotterknife.bindView
import javax.inject.Inject

class ContactInfoFragment : BasicFragment(), ContactInfoContract.View {

    companion object {
        private const val ID_KEY = "id key"
        fun newInstance(id: Int): ContactInfoFragment {
            val args = Bundle()
            args.putInt(ID_KEY, id)
            val fragment = ContactInfoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private val avatarImageView: CircleImageView by bindView(R.id.avatar)

    private val nameTextView: TextView by bindView(R.id.contact_name)

    private val phoneTextView: TextView by bindView(R.id.phone_number)

    private val genderTextView: TextView by bindView(R.id.gender)

    private val emailTextView: TextView by bindView(R.id.email)

    private val addressTextView: TextView by bindView(R.id.address)

    private val backButton: ImageView by bindView(R.id.back_button)

    private val loadingIndicator: ProgressBar by bindView(R.id.loading_indicator)

    private val messageButton: Button by bindView(R.id.message_button)


    @Inject
    lateinit var presenter: ContactInfoContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.contact_info_fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.init(arguments?.getInt(ID_KEY))
        backButton.setOnClickListener { presenter.backClicked() }
        messageButton.setOnClickListener { presenter.sendMessage() }
    }

    override fun exitFragment() {
        activity?.onBackPressed()
    }

    override fun setAvatar(avatar: String?) {
        Picasso.with(context)
                .load(avatar)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(avatarImageView)
    }

    override fun setName(name: String) {
        nameTextView.text = name
    }

    override fun setPhone(phone: String?) {
        phoneTextView.text = phone
    }

    override fun setGender(gender: String?) {
        genderTextView.text = gender
    }

    override fun setEmail(email: String?) {
        emailTextView.text = email
    }

    override fun setAddress(address: String?) {
        addressTextView.text = address
    }

    override fun displayLoadingIndicator() {
        loadingIndicator.visibility = View.VISIBLE
    }

    override fun hideLoadingIndicator() {
        loadingIndicator.visibility = View.GONE
    }

}
