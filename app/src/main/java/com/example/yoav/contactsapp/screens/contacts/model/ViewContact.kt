package com.example.yoav.contactsapp.screens.contacts.model

data class ViewContact(val id: Int?, val name: String, val avatarUrl: String?, val lastContacted: String?)