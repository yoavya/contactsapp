package com.example.yoav.contactsapp.screens.contacts

/**
 * Created by yoav on 02/02/2018
 */
interface ContactsContainerContract {
    interface View {
        fun initContactsPager()
    }

    interface Presenter {
        fun initContactsPager()

    }
}