package com.example.yoav.contactsapp.data.remote

import com.example.yoav.contactsapp.data.remote.model.RemoteDataContact
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by yoav on 03/02/2018
 */
interface ContactsApi {
    @GET("contacts_mock_short.json")
    fun getRemoteContacts(): Single<List<RemoteDataContact>>
}

