package com.example.yoav.contactsapp.di

import com.example.yoav.contactsapp.application.ContactsApplication
import com.example.yoav.contactsapp.communication.MessagingManager
import com.example.yoav.contactsapp.communication.PubNubManager
import com.example.yoav.contactsapp.data.ContactsRepository
import com.example.yoav.contactsapp.data.Repository
import com.example.yoav.contactsapp.data.local.LocalData
import com.example.yoav.contactsapp.data.local.LocalRealm
import com.example.yoav.contactsapp.data.remote.ApiFactory
import com.example.yoav.contactsapp.data.remote.RemoteData
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by yoav on 02/02/2018
 */
@Module
class AppModule {
    @Provides
    @Singleton
    fun providesApplication(app: ContactsApplication) = app

    @Provides
    @Singleton
    fun providesRepository(repo: ContactsRepository): Repository = repo

    @Provides
    @Singleton
    fun providesRemoteData(remoteData: ApiFactory): RemoteData = remoteData

    @Provides
    @Singleton
    fun providesLocalData(localData: LocalRealm): LocalData = localData

    @Provides
    @Singleton
    fun providesMessagingMenager(messagingManager: PubNubManager): MessagingManager = messagingManager

}