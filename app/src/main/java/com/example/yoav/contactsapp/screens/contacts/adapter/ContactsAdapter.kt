package com.example.yoav.contactsapp.screens.contacts.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.yoav.contactsapp.R
import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import com.squareup.picasso.Picasso
import io.reactivex.subjects.PublishSubject

class ContactsAdapter(var contacts: List<ViewContact>) : RecyclerView.Adapter<ContactViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false)
        return ContactViewHolder(view)
    }

    private var contactClickListener: PublishSubject<Int>? = null

    override fun getItemCount() = contacts.size

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val viewContact = contacts[position]
        Picasso.with(holder.itemView.context)
                .load(viewContact.avatarUrl)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.avatar)
        holder.name.text = viewContact.name
        holder.lastContacted.text = viewContact.lastContacted
        holder.itemView.setOnClickListener {
            val id = contacts[position].id
            if (id != null) {
                contactClickListener?.onNext(id)
            }
        }
    }

    fun setContactClickListener(contactClickListener: PublishSubject<Int>) {
        this.contactClickListener = contactClickListener
    }

}