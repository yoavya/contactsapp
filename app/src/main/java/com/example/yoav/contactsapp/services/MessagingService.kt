package com.example.yoav.contactsapp.services

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import com.example.yoav.contactsapp.R
import com.example.yoav.contactsapp.communication.MessagingManager
import com.pubnub.api.PubNub
import com.pubnub.api.callbacks.SubscribeCallback
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.pubsub.PNMessageResult
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult
import dagger.android.DaggerService
import java.util.*
import javax.inject.Inject


/**
 * Created by yoav on 04/02/2018
 */
class MessagingService : DaggerService() {

    @Inject
    lateinit var messagingManager: MessagingManager


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        messagingManager.init()
        messagingManager.subscribe()
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val listener: SubscribeCallback = object : SubscribeCallback() {
            override fun status(pubnub: PubNub?, status: PNStatus?) {}

            override fun presence(pubnub: PubNub?, presence: PNPresenceEventResult?) {}

            @SuppressLint("NewApi")
            override fun message(pubnub: PubNub?, message: PNMessageResult?) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val id = "my_channel_01"
                    val name = "my channel"
                    val description = "contacts app channel"
                    val importance = NotificationManager.IMPORTANCE_HIGH
                    val mChannel = NotificationChannel(id, name, importance)
                    mChannel.description = description
                    mChannel.enableLights(true)
                    mChannel.lightColor = Color.RED
                    mChannel.enableVibration(true)
                    mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                    notificationManager.createNotificationChannel(mChannel)
                }


                val notificationBuilder = NotificationCompat.Builder(applicationContext, UUID.randomUUID().toString())
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setContentTitle("New Message")
                        .setContentText(message?.message.toString())
                        .setChannelId("my_channel_01")
                notificationManager.notify(1, notificationBuilder.build())
            }

        }
        messagingManager.setListener(listener)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

}