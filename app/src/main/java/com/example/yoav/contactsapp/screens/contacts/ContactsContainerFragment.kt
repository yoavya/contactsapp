package com.example.yoav.contactsapp.screens.contacts

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.yoav.contactsapp.R
import com.example.yoav.contactsapp.screens.contacts.adapter.ContactsPagerAdapter
import com.example.yoav.contactsapp.screens.core.BasicFragment
import kotterknife.bindView
import javax.inject.Inject

class ContactsContainerFragment : BasicFragment(), ContactsContainerContract.View {
    companion object {

        fun newInstance() = ContactsContainerFragment()
    }

    @Inject
    lateinit var presenter: ContactsContainerContract.Presenter

    private val contactsPager: ViewPager by bindView(R.id.contact_pager)

    private val contactsTabs: TabLayout by bindView(R.id.contact_tabs)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.contacts_container_fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.initContactsPager()

    }

    override fun initContactsPager() {
        contactsPager.adapter = ContactsPagerAdapter(childFragmentManager)
        contactsTabs.setupWithViewPager(contactsPager)
    }
}
