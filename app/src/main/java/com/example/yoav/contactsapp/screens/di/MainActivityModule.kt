package com.example.yoav.contactsapp.screens.di

import com.example.yoav.contactsapp.di.PerActivity
import com.example.yoav.contactsapp.screens.MainActivity
import com.example.yoav.contactsapp.screens.MainContract
import com.example.yoav.contactsapp.screens.MainPresenter
import dagger.Binds
import dagger.Module

/**
 * Created by yoav on 02/02/2018
 */
@Module
abstract class MainActivityModule {

    @Binds
    @PerActivity
    abstract fun providesMainPresenter(presenter: MainPresenter): MainContract.Presenter

    @Binds
    abstract fun providesMainActivity(activity: MainActivity): MainContract.View
}