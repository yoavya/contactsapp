package com.example.yoav.contactsapp.screens.di

import com.example.yoav.contactsapp.screens.contacts.ContactsContainerFragment
import com.example.yoav.contactsapp.screens.contacts.ContactsFragment
import com.example.yoav.contactsapp.screens.contacts.di.ContactsContainerFragmentModule
import com.example.yoav.contactsapp.screens.contacts.di.ContactsFragmentModule
import com.example.yoav.contactsapp.screens.info.ContactInfoFragment
import com.example.yoav.contactsapp.screens.info.di.ContactInfoModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by yoav on 02/02/2018
 */
@Module
abstract class MainActivityFragmentProvider {

    @ContributesAndroidInjector(modules = [(ContactsContainerFragmentModule::class)])
    abstract fun providesContactsContainerFragment(): ContactsContainerFragment

    @ContributesAndroidInjector(modules = [(ContactsFragmentModule::class)])
    abstract fun providesContactsFragment(): ContactsFragment

    @ContributesAndroidInjector(modules = [(ContactInfoModule::class)])
    abstract fun providesContactInfoFragment(): ContactInfoFragment
}

