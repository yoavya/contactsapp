package com.example.yoav.contactsapp.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by yoav on 03/02/2018
 */

class RemoteDataContact {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("first_name")
    @Expose
    var firstName: String? = null
    @SerializedName("last_name")
    @Expose
    var lastName: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("gender")
    @Expose
    var gender: String? = null
    @SerializedName("phone")
    @Expose
    var phone: String? = null
    @SerializedName("address")
    @Expose
    var address: String? = null
    @SerializedName("job")
    @Expose
    var job: String? = null
    @SerializedName("avatar")
    @Expose
    var avatar: String? = null
    @SerializedName("date")
    @Expose
    var date: String? = null
}
