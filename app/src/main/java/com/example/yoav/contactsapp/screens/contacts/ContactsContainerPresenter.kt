package com.example.yoav.contactsapp.screens.contacts

import javax.inject.Inject

/**
 * Created by yoav on 02/02/2018
 */
class ContactsContainerPresenter @Inject constructor(val view: ContactsContainerContract.View) : ContactsContainerContract.Presenter {
    override fun initContactsPager() {
        view.initContactsPager()
    }


}

