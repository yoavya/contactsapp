package com.example.yoav.contactsapp.data

import com.example.yoav.contactsapp.data.local.LocalData
import com.example.yoav.contactsapp.data.local.model.LocalDataContact
import com.example.yoav.contactsapp.data.remote.RemoteData
import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import com.example.yoav.contactsapp.screens.info.model.ViewContactInfo
import io.reactivex.Observable
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ContactsRepository @Inject constructor(private val remoteRepo: RemoteData, private val localRepo: LocalData) :
        Repository {

    companion object {
        private const val MILLIS_IN_YEAR = 31536000000
        private const val MILLIS_IN_MONTH = 26288002880
        private const val MILLIS_IN_WEEK = 604800000
        private const val MILLIS_IN_DAY = 86400000
        private const val MILLIS_IN_HOUR = 3600000
        private const val MILLIS_IN_MINUTE = 60000
        private const val MILLIS_IN_SECOND = 1000
    }

    override fun updateContactDate(id: Int?) {
        localRepo.updateContactDateAsync(id, System.currentTimeMillis())
    }

    override fun getRecentContactsAsync(): Single<List<ViewContact>> {
        return localRepo.getContactsSortedByDateAsync()
                .flatMapObservable { getContactsSortedByDate(it) }
                .map { ViewContact(it.id, it.firstName + " " + it.lastName, it.avatar, getLastContactedString(it.date)) }
                .toList()
    }

    fun getLastContactedString(date: Long?): String? {
        if (date != null){
            val now = Calendar.getInstance().timeInMillis
            val diff = now - date
            return when {
                diff > MILLIS_IN_YEAR -> String.format(Locale.getDefault(), "%d years ago", diff/ MILLIS_IN_YEAR)
                diff > MILLIS_IN_MONTH -> String.format(Locale.getDefault(), "%d mon. ago", diff/ MILLIS_IN_MONTH)
                diff > MILLIS_IN_WEEK -> String.format(Locale.getDefault(), "%d weeks ago", diff/ MILLIS_IN_WEEK)
                diff > MILLIS_IN_DAY -> String.format(Locale.getDefault(), "%d days ago", diff/ MILLIS_IN_DAY)
                diff > MILLIS_IN_HOUR -> String.format(Locale.getDefault(), "%d hours ago", diff/ MILLIS_IN_HOUR)
                diff > MILLIS_IN_MINUTE -> String.format(Locale.getDefault(), "%d min. ago", diff/ MILLIS_IN_MINUTE)
                diff > MILLIS_IN_SECOND -> String.format(Locale.getDefault(), "%d sec. ago", diff/ MILLIS_IN_SECOND)
                else -> "now"
            }
        }
        return null
    }

    private fun getContactsSortedByDate(items: List<LocalDataContact>): Observable<LocalDataContact> {
        return if (items.isEmpty()) {
            getDataFromRemoteUpdateLocal()
                    .toList()
                    .flatMap { localRepo.getContactsSortedByDateAsync() }
                    .flatMapObservable { Observable.fromIterable(it) }
        } else {
            Observable.fromIterable(items)
        }
    }

    override fun getContactInfo(contactId: Int): Single<ViewContactInfo> {
        return localRepo.getContactInfoAsync(contactId)
                .map {
                    ViewContactInfo(it.id, it.avatar, it.firstName + " " + it.lastName, it.phone, it.gender, it
                            .email, it.address)
                }
    }

    override fun getContactsAsync(forceRefresh: Boolean): Single<List<ViewContact>> {
        return localRepo.getContactsAsync()
                .flatMapObservable { getContacts(it, forceRefresh) }
                .map { ViewContact(it.id, it.firstName + " " + it.lastName, it.avatar, null) }
                .toList()
    }

    private fun getContacts(items: List<LocalDataContact>, forceRefresh: Boolean = false): Observable<LocalDataContact> {
        return if (items.isEmpty() || forceRefresh) {
            getDataFromRemoteUpdateLocal()
        } else {
            Observable.fromIterable(items)
        }
    }

    private fun getDataFromRemoteUpdateLocal(): Observable<LocalDataContact> {
        return remoteRepo.getRemoteApi()
                .getRemoteContacts()
                .flatMapObservable { Observable.fromIterable(it) }
                .map { localRepo.saveContactSync(it) }
    }

}

