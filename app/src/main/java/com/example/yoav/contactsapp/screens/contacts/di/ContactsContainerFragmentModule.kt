package com.example.yoav.contactsapp.screens.contacts.di

import com.example.yoav.contactsapp.screens.contacts.ContactsContainerContract
import com.example.yoav.contactsapp.screens.contacts.ContactsContainerFragment
import com.example.yoav.contactsapp.screens.contacts.ContactsContainerPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class ContactsContainerFragmentModule {

    @Binds
    abstract fun providesContactsContainerPresenter(presenter: ContactsContainerPresenter): ContactsContainerContract.Presenter

    @Binds
    abstract fun proviesContactsContainerFragment(fragment: ContactsContainerFragment): ContactsContainerContract.View

}