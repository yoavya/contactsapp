package com.example.yoav.contactsapp.screens.info

import io.reactivex.disposables.Disposable

/**
 * Created by yoav_ on 03/02/2018
 */
interface ContactInfoContract {
    interface View {
        fun addDisposable(disposable: Disposable)
        fun setAvatar(avatar: String?)
        fun setName(name: String)
        fun setPhone(phone: String?)
        fun setGender(gender: String?)
        fun setEmail(email: String?)
        fun setAddress(address: String?)
        fun exitFragment()
        fun displayLoadingIndicator()
        fun hideLoadingIndicator()

    }
    interface Presenter{
        fun init(contactId: Int?)
        fun backClicked()
        fun sendMessage()

    }
}