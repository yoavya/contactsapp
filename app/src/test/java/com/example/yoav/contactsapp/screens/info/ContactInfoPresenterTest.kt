package com.example.yoav.contactsapp.screens.info

import com.example.yoav.contactsapp.communication.MessagingManager
import com.example.yoav.contactsapp.data.ContactsRepositoryTest
import com.example.yoav.contactsapp.data.Repository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test

/**
 * Created by yoav_ on 05/02/2018
 */
class ContactInfoPresenterTest {

    @MockK
    lateinit var view: ContactInfoContract.View

    @MockK
    lateinit var repo: Repository

    @MockK
    lateinit var messagingManager: MessagingManager

    lateinit var presenter: ContactInfoPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = ContactInfoPresenter(view, repo, messagingManager)
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun init() {
        every { view.displayLoadingIndicator() } just runs
        every { view.hideLoadingIndicator() } just runs
        val exampleViewContactInfo = ContactsRepositoryTest.exampleViewContactInfo
        every { repo.getContactInfo(any()) } returns Single.just(exampleViewContactInfo)
        every { view.setAvatar(any()) } just runs
        every { view.setName(any()) } just runs
        every { view.setPhone(any()) } just runs
        every { view.setGender(any()) } just runs
        every { view.setEmail(any()) } just runs
        every { view.setAddress(any()) } just runs
        every { view.addDisposable(ofType(Disposable::class)) } just runs
        presenter.init(1)
        verify { view.displayLoadingIndicator() }
        verify { view.hideLoadingIndicator() }
        verify { repo.getContactInfo(eq(1)) }
        verify { view.setAvatar(exampleViewContactInfo.avatar) }
        verify { view.setName(exampleViewContactInfo.name) }
        verify { view.setPhone(exampleViewContactInfo.phone) }
        verify { view.setGender(exampleViewContactInfo.gender) }
        verify { view.setEmail(exampleViewContactInfo.email) }
        verify { view.setAddress(exampleViewContactInfo.address) }
    }

    @Test
    fun sendMessage() {
        every { messagingManager.publish(any()) } just runs
        every { repo.updateContactDate(any()) } just runs
        presenter.contact = ContactsRepositoryTest.exampleViewContactInfo
        presenter.sendMessage()
        verify { messagingManager.publish(ofType(String::class)) }
        verify { repo.updateContactDate(eq(1)) }
    }

    @Test
    fun backClicked() {
        every { view.exitFragment() } just runs
        presenter.backClicked()
        verify { view.exitFragment() }
    }

}