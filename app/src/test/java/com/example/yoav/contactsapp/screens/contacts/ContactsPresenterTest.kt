package com.example.yoav.contactsapp.screens.contacts

import com.example.yoav.contactsapp.data.Repository
import com.example.yoav.contactsapp.screens.MainContract
import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.hamcrest.CoreMatchers.hasItem
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by yoav on 03/02/2018
 */
class ContactsPresenterTest {

    @MockK
    private
    lateinit var view: ContactsContract.View

    @MockK
    private
    lateinit var repo: Repository

    @MockK
    private
    lateinit var mainPresenter: MainContract.Presenter

    private lateinit var presenter: ContactsPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = ContactsPresenter(view, repo, mainPresenter)
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        every { view.displayLoadingIndicator() } just runs
        every { view.hideLoadingIndicator() } just runs
        every { view.addDisposable(any()) } just runs
        every { view.updateContacts(any()) } just runs
    }

    @Test
    fun initContacts_allContacts() {
        initContactsTestHelper(ContactsFragment.ALL_CONTACTS_TAB_TYPE)
    }

    @Test
    fun initContacts_recentContacts() {
        initContactsTestHelper(ContactsFragment.RECENT_TAB_TYPE)
    }

    private fun initContactsTestHelper(type: Int) {
        val contactsExample = listOf(ViewContact(1, "full name", "avatar url", "3 days ago"))
        val clickObservable = slot<PublishSubject<Int>>()
        every { view.getTabType() } returns type
        every { repo.getContactsAsync(false) } returns Single.just(contactsExample)
        every { repo.getRecentContactsAsync() } returns Single.just(contactsExample)
        every {
            view.setContactClickListener(contactClickListener = capture(clickObservable))
        } just runs
        presenter.initContacts()
        verify { view.addDisposable(ofType(Disposable::class)) }
        verify { view.updateContacts(contactsExample) }
        verify { view.setContactClickListener(any()) }
        val testObserver = TestObserver<Int>()
        clickObservable.captured.subscribe(testObserver)
        testObserver.onNext(1)
        assertThat(testObserver.values(), hasItem(1))
    }

    @Test
    fun refreshItems_AllContacts() {
        refreshItems(ContactsFragment.ALL_CONTACTS_TAB_TYPE)
    }

    @Test
    fun refreshItems_RecentContacts() {
        refreshItems(ContactsFragment.RECENT_TAB_TYPE)
    }

    private fun refreshItems(type: Int) {
        val contactsExample = listOf(ViewContact(1, "full name", "avatar url", "3 days ago"))
        every { view.getTabType() } returns type
        every { repo.getContactsAsync(true) } returns Single.just(contactsExample)
        every { view.stopRefresh() } just runs
        every { repo.getRecentContactsAsync() } returns Single.just(contactsExample)
        presenter.refreshItems()
        verify { view.addDisposable(ofType(Disposable::class)) }
        verify { view.updateContacts(contactsExample) }
        verify { view.stopRefresh() }
    }
}