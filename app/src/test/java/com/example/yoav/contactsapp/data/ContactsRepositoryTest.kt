package com.example.yoav.contactsapp.data

import com.example.yoav.contactsapp.data.local.LocalData
import com.example.yoav.contactsapp.data.local.model.LocalDataContact
import com.example.yoav.contactsapp.data.remote.RemoteData
import com.example.yoav.contactsapp.data.remote.model.RemoteDataContact
import com.example.yoav.contactsapp.screens.contacts.model.ViewContact
import com.example.yoav.contactsapp.screens.info.model.ViewContactInfo
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers.hasItem
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test


/**
 * Created by yoav_ on 03/02/2018
 */
class ContactsRepositoryTest {

    companion object {
        val exampleLocalContact by lazy {
            val exampleContact = LocalDataContact()
            exampleContact.id = 1
            exampleContact.firstName = "Yoav"
            exampleContact.lastName = "Yanuka"
            exampleContact.avatar = "someUrl"
            exampleContact.gender = "Male"
            exampleContact.phone = "12345678"
            exampleContact.email = "a@b.c"
            exampleContact.address = "123 st."
            exampleContact.date = 1000
            exampleContact
        }

        val example2LocalContact by lazy {
            val exampleContact = LocalDataContact()
            exampleContact.id = 2
            exampleContact.firstName = "Yo"
            exampleContact.lastName = "Ya"
            exampleContact.avatar = "someUrl"
            exampleContact.gender = "Male"
            exampleContact.phone = "12345678"
            exampleContact.email = "a@b.c"
            exampleContact.address = "123 st."
            exampleContact.date = 2000
            exampleContact
        }

        val exampleRemoteContact by lazy {
            val remoteDataContact = RemoteDataContact()
            remoteDataContact.id = 1
            remoteDataContact.firstName = "Yoav"
            remoteDataContact.lastName = "Yanuka"
            remoteDataContact.avatar = "someUrl"
            remoteDataContact.phone = "12345678"
            remoteDataContact.email = "a@b.c"
            remoteDataContact.gender = "Male"
            remoteDataContact.address = "123 st."
            remoteDataContact
        }

        val exampleViewContactInfo by lazy {
            ViewContactInfo(1, "someUrl", "Yoav Yanuka", "12345678", "Male", "a@b.c", "123 st.")
        }

        val contacts by lazy {
            listOf(exampleLocalContact, example2LocalContact)
        }
    }

    @MockK
    lateinit var localRepo: LocalData

    @MockK
    lateinit var remoteRepo: RemoteData

    lateinit var repo: ContactsRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        repo = ContactsRepository(remoteRepo, localRepo)
    }

    @Test
    fun getContactsAsync_NoRefresh() {
        getContactsAsync(false)
    }

    private fun getContactsAsync(forceRefresh: Boolean) {
        val testSubscriber = TestObserver<List<ViewContact>>()
        val resultList = listOf(ViewContact(1, "Yoav Yanuka", "someUrl", null))
        every { localRepo.getContactsAsync() } returns Single.just(listOf(exampleLocalContact))
        every { remoteRepo.getRemoteApi().getRemoteContacts() } returns Single.just(listOf(exampleRemoteContact))
        every { localRepo.saveContactSync(any()) } returns exampleLocalContact
        repo.getContactsAsync(forceRefresh).subscribe(testSubscriber)
        verify { localRepo.getContactsAsync() }
        testSubscriber.assertComplete()
        testSubscriber.assertNoErrors()
        assertThat(testSubscriber.values(), hasItem(resultList))
    }

    @Test
    fun getContactsAsync_Refresh() {
        getContactsAsync(true)
    }

    @Test
    fun getContactInfo() {
        val testSubscriber = TestObserver<ViewContactInfo>()
        every { localRepo.getContactInfoAsync(1) } returns Single.just(exampleLocalContact)
        repo.getContactInfo(1).subscribe(testSubscriber)
        verify { localRepo.getContactInfoAsync(1) }
        testSubscriber.assertComplete()
        testSubscriber.assertNoErrors()
        assertThat(testSubscriber.values(), hasItem(exampleViewContactInfo))
    }


    @Test
    fun getRecentContactsAsync() {
        val testObserver = TestObserver<List<ViewContact>>()
        every { localRepo.getContactsSortedByDateAsync() } returns Single.just(listOf(example2LocalContact, exampleLocalContact))
        val result = listOf(ViewContact(example2LocalContact.id, example2LocalContact.firstName + " " +
                example2LocalContact.lastName, example2LocalContact.avatar, repo.getLastContactedString(example2LocalContact.date)),
                ViewContact(exampleLocalContact.id, exampleLocalContact.firstName + " " + exampleLocalContact
                        .lastName, exampleLocalContact.avatar, repo.getLastContactedString(exampleLocalContact.date)))
        repo.getRecentContactsAsync().subscribe(testObserver)
        verify { localRepo.getContactsSortedByDateAsync() }
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        assertThat(testObserver.values(), hasItem(result))
    }

    @Test
    fun updateContactDate() {
        every { localRepo.updateContactDateAsync(any(), any()) } just runs
        repo.updateContactDate(1)
        verify { localRepo.updateContactDateAsync(eq(1), ofType(Long::class)) }
    }
}