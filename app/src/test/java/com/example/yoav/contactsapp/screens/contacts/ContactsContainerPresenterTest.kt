package com.example.yoav.contactsapp.screens.contacts

import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

/**
 * Created by yoav on 02/02/2018
 */
class ContactsContainerPresenterTest {

    @MockK
    lateinit var view: ContactsContainerContract.View

    lateinit var presenter: ContactsContainerPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = ContactsContainerPresenter(view)
    }

    @Test
    fun initContactsPager() {
        every { view.initContactsPager() } just runs
        presenter.initContactsPager()
        verify { view.initContactsPager() }
    }
}