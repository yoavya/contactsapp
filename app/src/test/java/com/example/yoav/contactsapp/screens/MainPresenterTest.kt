package com.example.yoav.contactsapp.screens

import com.example.yoav.contactsapp.screens.core.BasicFragment
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

/**
 * Created by yoav on 02/02/2018
 */
class MainPresenterTest {

    @MockK
    lateinit var view: MainContract.View

    lateinit var presenter: MainPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = MainPresenter(view)
    }

    @Test
    fun testInit() {
        every { view.swapToContractsFragment() } just runs
        presenter.init()
        verify { view.swapToContractsFragment() }
    }

    @Test
    fun swapFragment() {
        every { view.swapFragment(any()) } just runs
        val fragment = mockk<BasicFragment>()
        presenter.swapFragment(fragment)
        verify { view.swapFragment(fragment) }
    }
}